# Library
import pandas
import numpy
import statistics
import matplotlib.pyplot as pyplot
from sklearn.cluster import KMeans
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import MinMaxScaler

# --- Change here ---
# Dataset name
DATASET_NAME = 'Dataset_Clean.csv'

# K-Means
N_CLUSTER = 2
RANDOM_STATE = 123

# Saran Kuisioner
SARAN_K1 = ''
SARAN_K2 = ''
SARAN_K3 = ''
SARAN_K4 = ''
SARAN_K5 = ''
SARAN_K6 = ''
SARAN_K7 = ''
SARAN_K8 = ''
SARAN_K9 = ''
SARAN_K10 = ''
# --- END ---

def k_min_to_advice(index):
    if index == 1:
        return SARAN_K1
    elif index == 2:
        return SARAN_K2
    elif index == 3:
        return SARAN_K3
    elif index == 4:
        return SARAN_K4
    elif index == 5:
        return SARAN_K5
    elif index == 6:
        return SARAN_K6
    elif index == 7:
        return SARAN_K7
    elif index == 8:
        return SARAN_K8
    elif index == 9:
        return SARAN_K9
    elif index == 10:
        return SARAN_K10
    else:
        return ''

# Script Start here
dataset = pandas.read_csv(DATASET_NAME, sep=';')

print('--- Feature name ---')
print(dataset.columns.values)

print('\n--- Head of dataset ---')
print(dataset.head())
print('\n--- Describe of Dataset ---')
print('{}\n\n{}'.format(dataset.describe(), dataset.info()))

print('\n--- Check missing value ---')
print('{}\n\n{}'.format(dataset.isna().head(), dataset.isna().sum()))

# print('\n--- Print Scatter plot data ---')
pyplot.figure('Kusioner Kelas A')
pyplot.scatter(dataset['ID'], dataset['Rata_rata_KA'], alpha=0.5)
pyplot.xlabel('ID')
pyplot.ylabel('Rata-Rata Kuisioner Kelas A')
pyplot.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)
pyplot.figure('Kuisioner Kelas B')
pyplot.scatter(dataset['ID'], dataset['Rata_rata_KB'], alpha=0.5)
pyplot.xlabel('ID')
pyplot.ylabel('Rata-Rata Kuisioner Kelas B')
pyplot.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)

pyplot.figure('Scatter plot gabungan')
pyplot.scatter(dataset['ID'], dataset['Rata_rata_KA'], label='Kelas A', alpha=0.5, color='#2300A8')
pyplot.scatter(dataset['ID'], dataset['Rata_rata_KB'], label='Kelas B', alpha=0.5, color='#00A658')
pyplot.xlabel('ID')
pyplot.ylabel('Rata-Rata')
pyplot.legend(loc='best')
pyplot.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)

print('\n--- Clusttering ---')
dataset_cluster = dataset.drop(['ID', 'Nama', 'Nama_matkul_A', 'Kelompok_matkul_A', 'Total_responden_A', 'K1A', 'K2A', 'K3A', 'K4A', 'K5A', 'K6A', 'K7A', 'K8A', 'K9A', 'K10A', 'Total_KA', 'Kuisioner_Kelas_A', 'Nama_matkul_B', 'Kelompok_matkul_B', 'Total_responden_B', 'K1B', 'K2B', 'K3B', 'K4B', 'K5B', 'K6B', 'K7B', 'K8B', 'K9B', 'K10B', 'Kuisioner_Kelas_B', 'Total_KB', 'Total_KA_KB'], axis=1)
print(f'{dataset_cluster.head()}\n')
print('--- Data Normalization ---')
scaler = MinMaxScaler()
print(f'{scaler.fit_transform(dataset_cluster)}\n')

print('--- K-Means Algorithm ---')
kmeans = KMeans(n_clusters=N_CLUSTER, random_state=RANDOM_STATE)
print(f'{kmeans.fit(scaler.fit_transform(dataset_cluster))}\n')

print('--- Centroid Cluster ---')
centroid = scaler.inverse_transform(kmeans.cluster_centers_).tolist()
for index, centr in enumerate(centroid):
    print(f'Centroid {index} : X[{centr[0]}], Y[{centr[1]}]')
print()

print('--- Hasil Cluster ---')
print(f'{kmeans.labels_}\n')

dataset_cluster['Kluster'] = kmeans.labels_
pyplot.figure('Hasil Clustering dan Centroid')
pyplot.scatter(dataset['ID'], dataset['Total_KA_KB'], c=dataset_cluster['Kluster'], marker='o', alpha=0.5)
centers = scaler.inverse_transform(kmeans.cluster_centers_)
pyplot.scatter(centers[:,0], centers[:,1], c='red', alpha=1 , marker="*", label='Centroid')
pyplot.title("Hasil Klustering K-Means")
pyplot.legend(loc='best')
pyplot.grid(color='grey', linestyle='-', linewidth=0.25, alpha=0.5)

# Check lowest score
print('--- Check lowest score A class --- \n')
dataset_new = dataset.drop(['Total_responden_A', 'Total_KA', 'Rata_rata_KA', 'Kuisioner_Kelas_A', 'Total_responden_B', 'Kuisioner_Kelas_B', 'Total_KB', 'Rata_rata_KB', 'Total_KA_KB'], axis=1)
dataset_new['cluster'] = kmeans.labels_
for ID in dataset_new.values.tolist():
    print(ID)
    print(f'ID : {ID[0]}')
    print(f'Nama Dosen : {ID[1]}')
    print(f'--- A ---')
    print(f'Mata Kuliah : {ID[2]} [{ID[3]}]')
    print(f'Rata-Rata Skor : {statistics.mean(ID[4:13])}')
    print(f'K Terendah : {min(ID[4:13])} (K{ID.index(min(ID[4:13]))-3})')
    print(f'Saran : {k_min_to_advice(ID.index(min(ID[4:13]))-3)}')
    print(f'--- B ---')
    print(f'Mata Kuliah : {ID[14]} [{ID[15]}]')
    print(f'Rata-Rata Skor : {statistics.mean(ID[16:25])}')
    print(f'K Terendah : {min(ID[16:25])} (K{ID.index(min(ID[16:25]))-15})')
    print(f'Cluster : {ID[26]}')
    print(f'Saran : {k_min_to_advice(ID.index(min(ID[16:25]))-15)}')
    print()

pyplot.show()